#include<stdio.h>

int main()
{
    int K, N, M, X, Y, i;

    while(scanf("%d", &K) && K != 0){
        if(K>0 && K<=1000){
            scanf("%d%d", &N, &M);
            for(i=1; i<=K; ++i){
                scanf("%d%d", &X, &Y);

                if(X==N || Y==M)
                    printf("divisa\n");
                else if(X>N && Y>M)
                    printf("NE\n");
                else if(X<N && Y>M)
                    printf("NO\n");
                else if(X<N && Y<M)
                    printf("SO\n");
                else if(X>N && Y<M)
                    printf("SE\n");
            }
        }
        N=0, M=0;
    }

    return 0;
}
